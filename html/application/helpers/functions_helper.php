<?php if (! defined('BASEPATH')) exit('No direct script access allowed');


if(!function_exists('generate_callout')){
  
  
  function generate_callout(){
    
    $CI = &get_instance();
    $CI->load->library('session');

    $data['class']    = $CI->session->flashdata('callout_class');
    $data['message']  = $CI->session->flashdata('callout_message');

    $view = $CI->load->view('components/callout_tpl',$data,true);

    if($CI->session->flashdata('callout_message') != ""){

      echo $view;
    }
  } 
}

if(!function_exists('isLegacyBrowser')){
  
  
  function isLegacyBrowser(){
    
    $CI = &get_instance();
    $CI->load->library('user_agent');
    
    if ($CI->agent->browser() == 'Internet Explorer' and $CI->agent->version() <= 8){
    	return true;
    }else{
		return false;
    }
  }
}

if(!function_exists('is_current')){
	
	function is_current($uri,$page){
		
		if($uri == $page){
			
			echo 'current';
		}
		
	}
}

if(!function_exists('set_title')){
	
	function set_title($uri){
		
		$title = array(
			'home' => 'One Nations Enterprises International Inc.',
			'products' => 'Our Products',
			'about-us' => 'About Us',
			'contact-us' => 'Contact Us',
			'media'			=> 'Media',
			'account'	=> 'Account'
		);
		
		if(array_key_exists($uri,$title)){
			
			echo $title[$uri];
		}
		
	}
}

if(!function_exists('set_admin_breadcrumbs')){
	
	function set_admin_breadcrumbs($uri){
		$CI = &get_instance();
		
		$uri_list = array(
			'products' => 'fa-leaf',
			'account' => 'fa-user',
			'media'	=> 'fa-file-image-o'
		);
		
		if(array_key_exists($uri,$uri_list)){
			
			$data = array(
				'icon' => $uri_list[$uri],
				'heading'	=> ucwords($uri)
			);

			$tpl = $CI->load->view('components/admin_breadcrumbs_tpl',$data);
			
		}
		
		
	}
}

if(!function_exists('create_breadcrumbs')){
	
	function create_breadcrumbs(){
		
		$CI = &get_instance();
		
		$url_list = array(
			'home' => 'Home',
			'products' => 'Products',
			'about-us'	=> 'About Us',
			'contact-us' => 'Contact Us',
		);
		
		$total_segments = $CI->uri->total_segments();
		
		
		$links = "<p><a href='".base_url()."'>Home</a> <i class='fa fa-chevron-right'></i> ";
		
		for($i = 0; $i < $total_segments; $i++ ){
			$last_segment = $CI->uri->segment($i+1);
			
			if($i+1 == $total_segments){
				
				$item = $last_segment;
				
				if(array_key_exists($item,$url_list)){
					
					$item = $url_list[$last_segment];
				}else{
					
					$CI->load->model('product_model','product');
					
					if($product = $CI->product->get_by('slug',$last_segment)){
						
						$item = ucwords($product->product_name);
					}
				}
					 
					 
				$links .= "<span class='current'>".$item."</span>";
			}else{
				
				$links .= "<a href='".base_url().$CI->uri->segment($i+1)."'>".ucwords($url_list[$last_segment])."</a> <i class='fa fa-chevron-right'></i> ";
			}
		}
		
		
		$links .= "</p>";
		
		$view = $CI->load->view('components/breadcrumbs_tpl',array('links' =>$links),true);
		
		echo $view;
	}
}