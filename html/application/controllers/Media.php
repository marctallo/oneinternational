<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Media extends MY_Controller {

	
	public function __construct(){
		
		parent::__construct(true);
		$this->data['user'] = $this->ion_auth->user()->row();
		$this->load->model('product_model','product');
		$this->load->library('image_lib');

	}
  
	public function index(){

		$this->data['images'] = $this->scanImageDir();
		
		$this->template->load('templates/admin_tpl','media/index_tpl',$this->data);
	}
	
	public function add(){
											 
	
		$this->template->load('templates/admin_tpl','media/upload_tpl',$this->data);
	}
	
	public function upload(){
		
		$min_width = 320;
		$min_height = 320;
		$max_width = 1650;
		$max_height = 1650;
		$thumb_height = 320;
		$thumb_width = 320;
		
		
		$config['upload_path']          = $this->config->item('product_upload');;
		$config['allowed_types']        = 'jpeg|jpg|png';
		$config['max_size']             = 20000;
		$config['max_width']            = $max_width;
		$config['max_height']           = $max_height;
		$config['min_width']            = $min_width;
		$config['min_height']           = $min_height;
        $config['overwrite']            = TRUE;

		$this->load->library('upload', $config);
		//$this->upload->overwrite = true;

		if ( ! $this->upload->do_upload('media')){
			
			$this->output
				->set_status_header(500)
				->set_content_type('text/plain');
			
			$msg = $this->upload->display_errors('','');
			
			exit($msg);
		}else{
			
			$upload_data = $this->upload->data();
			
			$resize_conf = array(
					'source_image'  => $upload_data['full_path'], 
					'new_image'     => $upload_data['file_path']."thumbnail/".$upload_data['file_name'],
					'width'         => $thumb_width,
					'height'        => $thumb_height,
					'maintain_ration' => true
			);

			$this->image_lib->initialize($resize_conf);

				if ( ! $this->image_lib->resize()){
          exit($this->image_lib->display_errors());
      	}
			
			
		}
		
	}
	
	public function delete($filename){
		
		$path = $this->config->item('product_upload').$filename;
        $thumbnail_path = $this->config->item('product_upload')."thumbnail/".$filename;
		
		if(file_exists($path)){
			
            chmod($path, 0644);
			unlink($path);
          
            if(file_exists($thumbnail_path)){
              chmod($thumbnail_path, 0644);
              unlink($thumbnail_path);
            }
			
			$this->product->skip_validation();
			
			$this->product->update_by(array('image'=>$filename),array('image'=>'no-image.png'));
	
		}
		
		redirect('admin/media');
		
	}
	
	
	
	/**
		* Scan the image directory for the image files
		*
		* @return array
		*
		*/
	public function scanImageDir(){
		
		$path = $this->config->item('product_upload');
		
		if(is_dir($path)){
			
			$file_names = array_values(array_diff(scandir($path), array('..', '.','thumbnail')));
			
			return $file_names;
		}
		
		return false;
		
	}
	
	
	/**
		* Returns image names in json format
		*
		* @return json
		*
		*/
	public function getImageNames(){
		
		$images = $this->scanImageDir();
		
		
		if($images != false){
			
			$json_data = array();
			
			for($i=0;$i<count($images);$i++){
				
				$json_data[$i] = array($images[$i]);
			}
			
			return $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($json_data));
		}
		
		return false;
	}
	

}
