<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends MY_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->database();
		$this->load->library(array('ion_auth','form_validation'));
		$this->load->helper(array('url','language'));

		$this->form_validation->set_error_delimiters($this->config->item('error_start_delimiter', 'ion_auth'), $this->config->item('error_end_delimiter', 'ion_auth'));

		$this->lang->load('auth');
	}



	/**
		* login users
		*
		* @return void
		*
		*/
	public function login(){
		
		//check if the user is alreay logged in
		if($this->ion_auth->logged_in()){
			
			redirect('admin/products', 'refresh');
		}
		
		
		$this->data['title'] = $this->lang->line('login_heading');

		//validate form input
		$this->form_validation->set_rules('identity', str_replace(':', '', $this->lang->line('login_identity_label')), 'required');
		$this->form_validation->set_rules('password', str_replace(':', '', $this->lang->line('login_password_label')), 'required');

		if ($this->form_validation->run() == true){
		

			if ($this->ion_auth->login($this->input->post('identity'), $this->input->post('password')))
			{
				//if the login is successful
				//redirect them back to the home page
				$this->session->set_flashdata('message', $this->ion_auth->messages());
				redirect('admin/products');
			}
			else
			{
				// if the login was un-successful
				// redirect them back to the login page
				$this->session->set_flashdata('message', $this->ion_auth->errors());
				redirect('auth/login', 'refresh');
			}
		}else{
			// the user is not logging in so display the login page
			// set the flash data error message if there is one
			$this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');

			$this->data['identity'] = array('name' => 'identity',
				'id'    => 'identity',
				'type'  => 'text',
				'value' => $this->form_validation->set_value('identity'),
			);
			$this->data['password'] = array('name' => 'password',
				'id'   => 'password',
				'type' => 'password',
			);

			$this->template->load('templates/login_tpl','login/index_tpl', $this->data);
		}
	}

	/**
		* log users out of the admin page
		*
		* @return void
		*
		*/
	public function logout(){
		$this->data['title'] = "Logout";

		// log the user out
		$logout = $this->ion_auth->logout();

		// redirect them to the login page
		$this->session->set_flashdata('message', $this->ion_auth->messages());
		redirect('auth/login');
	}
	
	
	public function forgot_password(){
		
		$this->data['title'] = "Forgot Password";
		
		
		$this->data['identity'] = array('name' => 'identity',
				'id'    => 'identity',
				'type'  => 'text',
				'value' => $this->form_validation->set_value('identity'),
			);
		
		
		$this->form_validation->set_rules('identity', $this->lang->line('forgot_password_validation_email_label'), 'required|valid_email');
	
		if ($this->form_validation->run() == false){
			
			$this->data['type'] = $this->config->item('identity','ion_auth');

			$this->data['identity_label'] = $this->lang->line('forgot_password_email_identity_label');
			// set any errors and display the form
			$this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');
		}else{
			
			$identity_column = $this->config->item('identity','ion_auth');
			$identity = $this->ion_auth->where($identity_column, $this->input->post('identity'))->users()->row();
			
			if(empty($identity)) {
					
					$this->ion_auth->set_error('forgot_password_email_not_found');
					$this->data['message'] =  $this->ion_auth->errors();
					
			}else{
				
				$forgotten = $this->ion_auth->forgotten_password($identity->{$this->config->item('identity', 'ion_auth')});

				if ($forgotten){
					// if there were no errors
					$this->session->set_flashdata('message', $this->ion_auth->messages());
					redirect("auth/login"); //we should display a confirmation page here instead of the login page
				}else{
					$this->data['message'] =  $this->ion_auth->errors();
				}
			}
		}
		
		
		
		$this->template->load('templates/login_tpl','login/forgot_password_tpl', $this->data);
	}
	
	public function reset_password($code = NULL){
		
		$this->data['title'] = "Reset Password";
		
	if (!$code){
		
		show_404();
	}

		$user = $this->ion_auth->forgotten_password_check($code);

		if ($user){
			// if the code is valid then display the password reset form

			$this->form_validation->set_rules('new', $this->lang->line('reset_password_validation_new_password_label'), 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']|matches[new_confirm]');
			$this->form_validation->set_rules('new_confirm', $this->lang->line('reset_password_validation_new_password_confirm_label'), 'required');

			if ($this->form_validation->run() == false){
				// display the form

				// set the flash data error message if there is one
				$this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');

				$this->data['min_password_length'] = $this->config->item('min_password_length', 'ion_auth');
				$this->data['new_password'] = array(
					'name' => 'new',
					'id'   => 'new',
					'type' => 'password',
					'class' => 'input',
					'placeholder' => 'New Password',
					'pattern' => '^.{'.$this->data['min_password_length'].'}.*$',
				);
				$this->data['new_password_confirm'] = array(
					'name'    => 'new_confirm',
					'id'      => 'new_confirm',
					'type'    => 'password',
					'class' => 'input',
					'placeholder' => 'Confirm New Password',
					'pattern' => '^.{'.$this->data['min_password_length'].'}.*$',
				);
				$this->data['user_id'] = array(
					'name'  => 'user_id',
					'id'    => 'user_id',
					'type'  => 'hidden',
					'value' => $user->id,
				);
				
				$this->data['code'] = $code;

				// render
				$this->template->load('templates/login_tpl','login/reset_password_tpl', $this->data);
			}else{
				// do we have a valid request?
				if ($user->id != $this->input->post('user_id')){

					// something fishy might be up
					$this->ion_auth->clear_forgotten_password_code($code);

				}else{
					// finally change the password
					$identity = $user->{$this->config->item('identity', 'ion_auth')};

					$change = $this->ion_auth->reset_password($identity, $this->input->post('new'));

					if ($change){
						// if the password was successfully changed
						$this->session->set_flashdata('message', $this->ion_auth->messages());
						redirect("auth/login");
					}else{
						$this->session->set_flashdata('message', $this->ion_auth->errors());
						redirect('auth/reset_password/' . $code);
					}
				}
			}
		}else{
			// if the code is invalid then send them back to the forgot password page
			$this->session->set_flashdata('message', $this->ion_auth->errors());
			redirect("auth/forgot_password");
		}
	}
	

}
