<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Products extends MY_Controller {

	
	public function __construct(){
		
      parent::__construct(true);
		
      $this->load->library('form_validation');
      $this->load->model('product_model','product');
      $this->data['user'] = $this->ion_auth->user()->row();
      $this->form_validation->set_error_delimiters('<p class="error-msg">', '</p>');
	}
	
	public function index(){
		
		$this->load->library('pagination');
		
		$config['base_url'] = base_url().'admin/products';
		$config['total_rows'] = $this->product->getTotal();
		$config['per_page'] = 20;
		
		$this->pagination->initialize($config);
		
		$offset = $this->uri->segment(3,0);
		
		$this->data['products'] = $this->product->paginate($config['per_page'],$offset);

		$this->template->load('templates/admin_tpl','products/index_tpl',$this->data);
	}
	
	public function add(){
      
        $data = array(
			'product_name' 	=> $this->input->post('product_name'),
			'slug'					=> $this->input->post('slug'),
			'description'		=> $this->input->post('description'),
			'image'					=> ($this->input->post('image') == '') ? 'no-image.png' : $this->input->post('image')
		);
		
		$this->product->validate[0]['rules'] .= "|is_unique[products.product_name]";
		$this->product->validate[1]['rules'] .= "|is_unique[products.slug]";

		
		$this->form_validation->set_rules($this->product->validate);
		

		if (!$this->form_validation->run() == FALSE){
				
			
			$return_id = $this->product->insert(html_purify($data));
			
			if($return_id != false){
				
				$this->session->set_flashdata('callout_class', 'success');
      	        $this->session->set_flashdata('callout_message', 'A new product has been added.');
				
			}else {
				
				$this->session->set_flashdata('callout_class', 'alert');
      	        $this->session->set_flashdata('callout_message', 'Soemthing went wrong.');
				
			}
			
			
          
			redirect('admin/products');
		}
		
		$this->template->load('templates/admin_tpl','products/edit_tpl',$this->data);
	}
	
	
	public function edit($id){
		
		$product = $this->product->get($id);
		
		$this->data['product'] = $product;
      
        $data = array(
                'product_name' 	=> $this->input->post('product_name'),
                'slug'			=> $this->input->post('slug'),
                'description'	=> $this->input->post('description'),
                'image'			=>  $this->input->post('image')
        );
		
		$this->product->validate[0]['rules'] .= "|callback_edit_unique[products.product_name.".$id."]";
		$this->product->validate[1]['rules'] .= "|callback_edit_unique[products.slug.".$id."]";
    
		$this->form_validation->set_message('edit_unique', 'Sorry, that %s is already being used.');
        $this->form_validation->set_rules($this->product->validate);
		

		if (!$this->form_validation->run() == FALSE){
		
			$return_id = $this->product->update($id, html_purify($data));
			
			if($return_id != false){
				
				$this->session->set_flashdata('callout_class', 'success');
				$this->session->set_flashdata('callout_message', 'Product has been updated.');	
			}else {
				
				$this->session->set_flashdata('callout_class', 'alert');
				$this->session->set_flashdata('callout_message', 'Something went wrong');
			}
			
			
			redirect('admin/products');
		}
		
		$this->template->load('templates/admin_tpl','products/edit_tpl',$this->data);
	}
	

	
	public function delete($id){
		
		$this->product->delete($id);
      
        $this->session->set_flashdata('callout_class', 'alert');
        $this->session->set_flashdata('callout_message', 'Product has been deleted.');
		
		redirect('admin/products');
	}
	
	public function edit_unique($value, $params) {


			list($table, $field, $current_id) = explode(".", $params);

			$query = $this->db->select()->from($table)->where($field, $value)->limit(1)->get();

			if ($query->row() && $query->row()->id != $current_id){
					return FALSE;
			}
	}
  

}