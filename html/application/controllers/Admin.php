<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends MY_Controller {

   public function __construct(){
      
    	parent::__construct(true);
        $this->data['user'] = $this->ion_auth->user()->row();
        $this->load->library('form_validation');
        $this->form_validation->set_error_delimiters('<p class="error-msg">', '</p>');
   }
  
  
	
	public function account(){
		
		$user = $this->data['user'];
		// validate form input
		$this->form_validation->set_rules('email', 'Email', 'required|valid_email');
		$this->form_validation->set_rules('first_name', 'First Name', 'required');
		$this->form_validation->set_rules('last_name', 'Last Name', 'required');


		if (isset($_POST) && !empty($_POST)){
			
			// update the password if it was posted
			if ($this->input->post('password')){
				$this->form_validation->set_rules('password', 'Password', 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']|matches[password_confirm]');
				$this->form_validation->set_rules('password_confirm','Confirm Password', 'required');
			}

			if ($this->form_validation->run() === TRUE){
				
				$data = array(
					'email'			 => $this->input->post('email'),
					'first_name' => $this->input->post('first_name'),
					'last_name'  => $this->input->post('last_name')
				);

				// update the password if it was posted
				if ($this->input->post('password')){
					$data['password'] = $this->input->post('password');
				}

			// check to see if we are updating the user
			  if($this->ion_auth->update($user->id, html_purify($data))){
			    	
					$this->session->set_flashdata('callout_class', 'success');
					$this->session->set_flashdata('callout_message', 'Account Updated.');
					
					redirect('admin/account');
				 
			  }else{
			    
					$this->session->set_flashdata('callout_class', 'success');
				  $this->session->set_flashdata('callout_message', $this->ion_auth->errors() );
			 	}	
			}
		}
		
		$this->template->load('templates/admin_tpl','account/edit_tpl',$this->data);
	}
	
}