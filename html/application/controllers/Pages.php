<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pages extends MY_Controller {

  public $data;
  private $cache_time = 60;

  public function __construct(){

      parent::__construct();

      $this->data['is_legacy'] = isLegacyBrowser();
      $this->load->model('product_model','product');

      // $this->output->enable_profiler(TRUE);
  }
	
  public function index(){
    $this->output->cache($this->cache_time);
    $this->template->load('templates/main_tpl','pages/home_tpl',$this->data);
  }
  
  public function products(){
    
    $this->data['list'] = $this->product->generateProductList();
		
    $this->template->load('templates/main_tpl','pages/product_list_tpl',$this->data);
  }
  
  public function about_us(){
     
     $this->output->cache($this->cache_time);
     $this->template->load('templates/main_tpl','pages/about_us_tpl',$this->data);
  }
  
  public function contact_us(){
    
    $this->output->cache($this->cache_time);
    $this->template->load('templates/main_tpl','pages/contact_us_tpl',$this->data);
    
  }
  
  public function single($slug){
		
		$product = $this->product->get_by('slug',$slug);
    
        if($product == false){
          
          show_404();
        }
		
		$this->data['product'] = $product;

    
    $this->template->load('templates/main_tpl','pages/single_product_tpl',$this->data);
  }

	

}
