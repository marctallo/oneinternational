<?php
defined('BASEPATH') OR exit('No direct script access allowed');


$config['full_tag_open'] = '<ul class="pagination text-right" role="navigation" aria-label="Pagination">';
$config['full_tag_close'] = '</ul>';
$config['num_tag_open'] = '<li>';
$config['num_tag_close'] = '</li>';
$config['first_tag_open'] = '<li>';
$config['first_tag_close'] = '</li>';
$config['last_tag_open'] = '<li>';
$config['last_tag_close'] = '</li>';
$config['prev_tag_open'] = '<li class="pagination-previous">';
$config['prev_tag_close'] = '</li>';
$config['next_tag_open'] = '<li class="pagination-next">';
$config['next_tag_close'] = '</li>';
$config['cur_tag_open'] = '<li class="current">';
$config['cur_tag_close'] = '</li>';
$config['first_link'] = 'First';
$config['last_link'] = 'Last';
$config['next_link'] = 'Next';
$config['prev_link'] = 'Previous';