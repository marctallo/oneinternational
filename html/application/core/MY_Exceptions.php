<?php

class MY_Exceptions extends CI_Exceptions{

    public function __construct(){
        parent::__construct();
    }

    public function show_404($page='',$log_error = true){    
   
			
			$CI =& get_instance();
        $CI->load->view('pages/custom_404_tpl');
        echo $CI->output->get_output();
        exit;
    }

}
