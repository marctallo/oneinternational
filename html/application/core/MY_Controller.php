<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Controller extends CI_Controller {
  
  public $data;

  public function __construct($auth = false){

    parent::__construct();

    $this->load->library('template');
    $this->load->library('ion_auth');
		
		
		//check if the controller needs authentication
		if($auth){
			
			if(!$this->ion_auth->logged_in()){
				
				show_404();
			}
		}
   
  }
  
	
}
