<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Product_model extends MY_Model {
	
	public $_table = 'products';
	public $protected_attributes = array( 'id');
	public $before_create = array( 'created_at', 'updated_at' );
  public $before_update = array( 'updated_at' );
	public $validate = array(
        array( 'field' => 'product_name', 
               'label' => 'Product Name',
               'rules' => 'required' ),
        array( 'field' => 'slug',
               'label' => 'Slug',
               'rules' => 'required|alpha_dash')
	);
  

	
	
	/**
		* Generate product list
		*
		* @return string
		*
		*/
	public function generateProductList(){
		
		$per_row = 4;
		$products = self::get_all();
		$total = count($products);
		$counter = 1;
		$list ="<div class='row product-row'>";
		

		foreach($products as $product){
			
			// add end class for the last column. foundation 6 fix
			$product->is_last = ($total == $counter) ? 'end' : '';
			
			//convert tempalte to string
			$tpl = $this->load->view('products/product_tpl',$product,true);
			
			//add the template
			$list .=$tpl;
			
			//check if counter if divisible by per_row
			if(($counter%$per_row) == 0){
				
				//check if the it is the last item on the list and close the div
				if($total == $counter){
				
					$list .="</div>";
				}else{
					$list .="</div><div class='row product-row'>";
					
				}
				
			}
			$counter++;
		}
		
		return $list;
	}
	
	
	public function getTotal(){
		
		return count(self::get_all());
		
	}
	
	public function paginate($limit,$offset){
		
		$products = self::order_by('id','desc')->limit($limit,$offset)->get_all();
		
		return $products;
		
	}
	
	
	
}
