<div class="account-details">
	<?php echo form_open(); ?>
	<?php echo form_hidden('id', $user->id);?>
	<div class="row">
     <div class="small-3 columns">
       <label class="text-right middle">Email</label>
      </div>
      <div class="small-9 medium-6 columns end">
          <?php echo form_input("email",(set_value("email")!="") ? set_value("email") :(isset($user->email) ? $user->email : ''),"placeholder='Email Address'"); ?>
        <?php echo form_error('email'); ?>
      </div>
    </div>
    <div class="row">
     <div class="small-3 columns">
       <label class="text-right middle">First Name</label>
      </div>
      <div class="small-9 medium-6 columns end">
          <?php echo form_input("first_name",(set_value("first_name")!="") ? set_value("first_name") :(isset($user->first_name) ? $user->first_name : ''),"placeholder='First Name'"); ?>
        <?php echo form_error('first_name'); ?>
      </div>
    </div>
    <div class="row">
     <div class="small-3 columns">
       <label class="text-right middle">Last Name</label>
      </div>
      <div class="small-9 medium-6 columns end">
          <?php echo form_input("last_name",(set_value("last_name")!="") ? set_value("last_name") :(isset($user->last_name) ? $user->last_name : ''),"placeholder='Last Name'"); ?>
        <?php echo form_error('last_name'); ?>
      </div>
    </div>
    <div class="row">
     <div class="small-3 columns">
       <label class="text-right middle">Password</label>
      </div>
      <div class="small-9 medium-6 columns end">
          <?php echo form_password("password",'',"placeholder='Password'"); ?>
           <?php echo form_error('password'); ?>
      </div>
    </div>
    <div class="row">
     <div class="small-3 columns">
       <label class="text-right middle">Confirm Password</label>
      </div>
      <div class="small-9 medium-6 columns end">
          <?php echo form_password("password_confirm",'',"placeholder='Confirm Password'"); ?>
           <?php echo form_error('password_confirm'); ?>
        
      </div>
    </div>
    <div class="row">
    	<div class="small-12  medium-9 medium-centered columns end">
    		<input type="submit" class="float-right button" value="update">
    	</div>
    </div>
  <?php echo form_close(); ?>
</div> 		