<div class="media-upload">
	<div class="row">
		<div class="small-12 columns">
			<form action="<?php echo base_url(); ?>media/upload" class="dropzone" id="image-dropzone">
				<input type="hidden" name="<?=$this->security->get_csrf_token_name();?>" value="<?=$this->security->get_csrf_hash();?>" />		
			</form>
		</div>
	</div>
	<div class="row">
		<div class="small-12 columns">
			<p>PNG, JPG, JPEG formats are allowed(maxsize of 20MB).</p>
		</div>
	</div>
	<div class="row">
		<div class="small-12 columns">
			<p>Images must be at least 320x320 pixels and not more than 1650x1650 pixels.</p>
		</div>
	</div>
</div>