<div class="media-list">
	
	<div class="row">
		<div class="small-12 columns">
			<table>
				<thead>
					<tr>
						<th>Media Image</th>
						<th>Media Name</th>
						<th></th>
					</tr>
				</thead>
				<tbody>
					<?php if($images == false) : ?>
					
						<tr>
							<td colspan="3" class="text-center">No available images</td>
						</tr>
					
					<?php else : ?>
					
						<?php foreach($images as $v) : ?>
							<tr>
								<td><img class="media-thumb" src="<?php echo base_url().'assets/images/products/'.$v; ?>" alt="<?php echo $v; ?>"> </td>
								<td><?php echo $v; ?></td>
								<td><a href="<?php echo base_url()."media/delete/".$v; ?>" onclick="return confirm('Delete this image?')"><i class="fa fa-trash"></i></a></td>
							</tr>
							
						<?php endforeach; ?>
					
					<?php endif; ?>
					
				</tbody>
			</table>
			<a href="<?php echo base_url(); ?>admin/media/add">Add Image</a>
		</div>
	</div>
</div>