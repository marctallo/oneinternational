<!doctype html>
<html class="no-js" lang="en">

<head>
  <meta charset="utf-8" />
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <title>Home</title>
  
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/f4/normalize.css" />
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/f4/foundation-3.min.css" />
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/f4/ie8-grid-foundation-4.css" />
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/font-awesome.min.css" />
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/style.css" />
  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/f4/respond.min.js"></script>

</head>

<body>
  <header class="main-header">
    <div class="social-media-box">
      <div class="row">
        <div class="twelve small-12 columns">
          <p class="tagline">ONE road to success</p>
          <ul class="social-icons">
            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
            <li><a href="#"><i class="fa fa-instagram"></i></a></li>
          </ul>
        </div>
      </div>

    </div>
    <div class="navigation">
      <div class="row">
        <div class="twelve small-12 columns">
          <div class="logo">
            <img src="<?php echo base_url(); ?>assets/images/nav-logo.png" alt="ONE International">
          </div>
          <div class="mobile-menu">
            <img src="<?php echo base_url(); ?>assets/images/icon-menu.svg"/>
          </div>
          <nav class="main-nav">
            <ul class="menu">
              <li><a href="#">Home</a></li>
              <li><a href="#">Products</a></li>
              <li><a href="#">About Us</a></li>
              <li><a href="#">Contact Us</a></li>
            </ul>
          </nav>
        </div>
      </div>
    </div>
  </header>
  <main class="body-content">
  	<section class="company-vision">
    <div class="row">
      <div class="small-12 four medium-4 columns content-box">
        <i class="fa fa-group"></i>
        <h3>Mission</h3>
        <p>Our mission is to be a well known brand and globally recognised for beauty products and organic health effectiveness.</p>
      </div>
      <div class="small-12 four medium-4 columns content-box">
        <i class="fa fa-flag"></i>
        <h3>Vision</h3>
        <p>Our vision is to be one of the leading globally recognised companies in promoting beauty products of organic and natural origin.</p>
      </div>
      <div class="small-12 four medium-4 columns content-box">
        <i class="fa fa-leaf"></i>
        <h3>Products</h3>
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquid fugiat sed voluptates error pariatur, dolorem, corrupti voluptatum accusantium odio hic. Amet iste laborum vitae assumenda nostrum architecto atque, ab eaque.</p>
      </div>
    </div>
  </section>
  </main>
  <footer class="footer">
    <div class="row">
      <div class="small-12 four medium-4 columns content-box">
        <div class="logo">
          <img src="<?php echo base_url(); ?>assets/images/footer-logo.png" alt="ONE International">
        </div>
        <ul class="social-icons">
          <li><a href="#"><i class="fa fa-facebook"></i></a></li>
          <li><a href="#"><i class="fa fa-twitter"></i></a></li>
          <li><a href="#"><i class="fa fa-instagram"></i></a></li>
        </ul>
      </div>
      <div class="small-12 four medium-4 columns content-box">
        <p class="nav-header"><strong>Navigation Links</strong></p>
        <ul class="footer-nav">
          <li><a href="#">Home</a></li>
          <li><a href="#">Products</a></li>
          <li><a href="#">About Us</a></li>
          <li><a href="#">Contact Us</a></li>
        </ul>
      </div>
      <div class="small-12 four medium-4 columns content-box">
        <p class="nav-header"><strong>Contact Us</strong></p>
        <ul class="contact-nav">
          <li><a href="#" class="company-name">One Nations Enterprises International Inc.</a></li>
          <li><a href="#" class="company-address"><i class="fa fa-map-marker"></i> GF Delta Bldg., West Avenue, Quezon City</a></li>
          <li><a href="#" class="company-number"><i class="fa fa-phone"></i> 0945-3456456 , 0456-0596856</a></li>
          <li><a href="#" class="company-email"><i class="fa fa-envelope"></i> email@email.com</a></li>
        </ul>
      </div>
    </div>
    <div class="copyright-box">
      <div class="row">
        <div class="small-12 columns">
          <p class="copyright">© One Nations Enterprises International Inc.</p>
        </div>
    </div>
    </div>
  </footer>

</body>

</html>