<!DOCTYPE html>
	<html>
	<head>
		<title><?php echo $title; ?></title>
	</head>
	<link rel="icon" type="image/png" sizes="32x32" href="<?php echo base_url(); ?>assets/images/favicon-32x32.png">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/foundation.min.css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/font-awesome.min.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/login/style.css">
	<body>
		<div class="login-box">
			<?php if(isset($message)) : ?>
			<div class="error-msg"><?php echo $message;?></div>
			<?php endif; ?>
			<?php echo $body; ?>
		</div>
		<script src="<?php echo base_url(); ?>assets/js/jquery.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/foundation.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/jquery.colorbox-min.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/main.js"></script>
	</body>
	</html>	