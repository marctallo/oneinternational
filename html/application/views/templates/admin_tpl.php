
<!doctype html>
<html class="no-js" lang="en">

<head>
	<meta charset="utf-8" />
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<title><?php echo set_title($this->uri->segment(2)); ?></title>
	<link rel="icon" type="image/png" sizes="32x32" href="<?php echo base_url(); ?>assets/images/favicon-32x32.png">
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/foundation.min.css" />
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/font-awesome.min.css" />
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/dropzone.css" />
	<!--<link href="https://fonts.googleapis.com/css?family=Lato:400,700" rel="stylesheet">-->
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/admin/style.css" />
</head>

<body>
	<div class="body-wrap">
		<header class="admin-header">
			<div class="title-bar" data-responsive-toggle="top-menu" data-hide-for="medium" >
				<button class="menu-icon" type="button" data-toggle></button>
				<div class="title-bar-title">Menu</div>
			</div>

			<div class="top-bar" id="top-menu">
				<div class="top-bar-left">
					<div class="logo hide-for-small-only">
						<img src="<?php echo base_url(); ?>assets/images/nav-logo.png" alt="One Nations International">
					</div>
				</div>
				<div class="top-bar-right">
					<ul class="dropdown menu" data-dropdown-menu>
						<li>
							<a href="#"><i class="fa fa-user"></i> <?php echo ucwords($user->first_name); ?></a>
							<ul class="menu vertical">
								<li><a href="<?php echo base_url(); ?>auth/logout">logout</a></li>
							</ul>
						</li>
					</ul>
				</div>
			</div>
		</header>
		<div class="row expanded">
			<aside class="small-12 medium-2 columns sidebar">
				<div class="user-info">
					<p class="user-name"><?php echo ucwords($user->first_name)." ".ucwords($user->last_name); ?></p>
					<p class="user-last-login"><?php echo date("F d, Y G:i",$user->last_login); ?></p>
				</div>
				<ul class="main-menu">
					<li><a class="<?php is_current($this->uri->segment(2),'products'); ?>" href="<?php echo base_url(); ?>admin/products"><i class="fa fa-leaf"></i>Products</a></li>
					<li><a class="<?php is_current($this->uri->segment(2),'account'); ?>" href="<?php echo base_url(); ?>admin/account"><i class="fa fa-user"></i>Account</a></li>
					<li><a class="<?php is_current($this->uri->segment(2),'media'); ?>" href="<?php echo base_url(); ?>admin/media"><i class="fa fa-file-image-o"></i>Media</a></li>
				</ul>

			</aside>
			<main class="small-12 medium-10 columns main-content">
				<div class="row">
					<?php set_admin_breadcrumbs($this->uri->segment(2)); ?>
				</div>
				<div class="row">
				  <?php generate_callout(); ?>
				</div>
				<div class="inner-content">
					<?php echo $body; ?>
				</div>
			</main>
		</div>

		<footer class="admin-footer"></footer>
		
		<!-- modal -->
		<div class="large reveal" id="image-select-modal" data-reveal data-animation-in="fade-in">
			<div class="modal-header">
				<p>Choose Product Image</p>
			</div>
			<div class="modal-content">
				<div class="loader">
					<img src="<?php echo base_url(); ?>assets/images/rolling.gif" alt="loading">
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" id="set-image">select</button>
			</div>
			<button class="close-button" data-close aria-label="Close modal" type="button">
			<span aria-hidden="true">&times;</span>
		</button>
</div>
	</div>
	<input type="hidden" id="base_url" value="<?php echo base_url(); ?>">
	<script src="<?php echo base_url(); ?>assets/js/jquery.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/foundation.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/tinymce.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/dropzone.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/admin/main.js"></script>
</body>

</html>