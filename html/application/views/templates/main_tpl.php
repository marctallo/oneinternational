<!doctype html>
<html class="no-js" lang="en">

<head>
  <meta charset="utf-8" />
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <title><?php set_title($this->uri->segment(1,'home')); ?></title>
  <link rel="icon" type="image/png" sizes="32x32" href="<?php echo base_url(); ?>assets/images/favicon-32x32.png">
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/foundation.min.css" />
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/font-awesome.min.css" />
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/colorbox.css" />
 <!-- <link href="https://fonts.googleapis.com/css?family=Lato:400,700" rel="stylesheet">-->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/style.css" />
	<!--[if lt IE 9]>
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/normalize.css" />
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/foundation-3.min.css" />
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
   <![endif]-->
</head>

<body>
  
  <header class="main-header">
    <div class="social-media-box">
      <div class="row">
        <div class="twelve small-12 columns">
          <!--<p class="tagline">ONE road to success</p>-->
          <ul class="social-icons">
            <li><a href="https://www.facebook.com/Onenationsinternational" target="_blank"><i class="fa fa-facebook-official"></i> Like us on Facebook</a></li>
            <li><a class="user-login" href="http://oneintlinc.com/back_office/"><i class="fa fa-user"></i> Premium Login</a></li>
            <li><a class="user-login" href="http://silver.oneintlinc.com/back_office"><i class="fa fa-user"></i> Silver Login</a></li>
          </ul>
          
        </div>
      </div>

    </div>
    <div class="navigation">
      <div class="row">
        <div class="twelve small-12 columns">
          <div class="logo">
            <img src="<?php echo base_url(); ?>assets/images/nav-logo.png" alt="ONE International"> <p class="tagline">ROAD TO SUCCESS</p>
          </div>
          <div class="mobile-menu">
            <img src="<?php echo base_url(); ?>assets/images/icon-menu.svg"/>
          </div>
          <nav class="main-nav">
           
            <ul class="menu">
              <li><a href="<?php echo base_url(); ?>" class="<?php is_current($this->uri->segment(1,'home'),'home'); ?>">Home</a></li><!--
              --><li><a href="<?php echo base_url(); ?>products"  class="<?php is_current($this->uri->segment(1),'products'); ?>">Products</a></li><!--
              --><li><a href="<?php echo base_url(); ?>about-us"  class="<?php is_current($this->uri->segment(1),'about-us'); ?>">About Us</a></li><!--
              --><li><a href="<?php echo base_url(); ?>contact-us"  class="<?php is_current($this->uri->segment(1),'contact-us');?>">Contact Us</a></li>
            </ul>
          </nav>
        </div>
      </div>
    </div>
  </header>
  <main class="body-content">
  <?php if($this->uri->segment(1,"home") != "home"):?>
		 <?php echo create_breadcrumbs(); ?>
  <?php endif; ?>
   <?php echo $body; ?>
  </main>
  <footer class="footer">
    <div class="row">
      <div class="four small-12 medium-4 columns content-box">
        <div class="logo">
          <img src="<?php echo base_url(); ?>assets/images/footer-logo.png" alt="ONE International">
        </div>
        <ul class="social-icons">
          <li><a href="https://www.facebook.com/Onenationsinternational" target="_blank"><i class="fa fa-facebook-official"></i> Like us on Facebook</a></li>
        </ul>
      </div>
      <div class="four small-12 medium-4 columns content-box">
        <p class="nav-header"><strong>Navigation Links</strong></p>
        <ul class="footer-nav">
          <li><a href="<?php echo base_url(); ?>">Home</a></li>
          <li><a href="<?php echo base_url(); ?>products">Products</a></li>
          <li><a href="<?php echo base_url(); ?>about-us">About Us</a></li>
          <li><a href="<?php echo base_url(); ?>contact-us">Contact Us</a></li>
        </ul>
      </div>
      <div class="four small-12 medium-4 columns content-box">
        <p class="nav-header"><strong>Contact Us</strong></p>
        <ul class="contact-nav">
          <li><a href="#" class="company-name">One Nations Enterprises International Inc.</a></li>
          <li><a href="#" class="company-address"><i class="fa fa-map-marker"></i> Unit 9 GF N. Dela Merced Bldg., West Avenue, Quezon City</a></li>
          <li><a href="mailto:oneintlinc@gmail.com" class="company-email"><i class="fa fa-envelope"></i> oneintlinc@gmail.com</a></li>
        </ul>
      </div>
    </div>
    <div class="copyright-box">
      <div class="row">
        <div class="small-12 columns">
          <p class="copyright">© One Nations Enterprises International Inc.</p>
        </div>
    </div>
    </div>
  </footer>
  <?php if(!$is_legacy) : ?>
		<script src="<?php echo base_url(); ?>assets/js/jquery.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/foundation.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/jquery.colorbox-min.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/main.js"></script>
	<?php else : ?>
		<script src="<?php echo base_url(); ?>assets/js/jquery-1.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/foundation-3.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/jquery.colorbox-min.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/ie8.js"></script>
	<?php endif; ?>
</body>

</html>