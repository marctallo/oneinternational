<!DOCTYPE html>
	<html>
	<head>
		<title><?php echo $title; ?></title>
	</head>
	<link rel="icon" type="image/png" sizes="32x32" href="<?php echo base_url(); ?>assets/images/favicon-32x32.png">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/foundation.min.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/login/style.css">
	<body>
		<div class="login-box">
			<?php if(isset($message)) : ?>
			<p class="error-msg"><?php echo $message;?></p>
			<?php endif; ?>
			<?php echo form_open("auth/login");?>
				<img src="<?php echo base_url(); ?>assets/images/logo.png" alt="">
				<?php echo form_input($identity,null,"class='input' placeholder='Email'");?>
				<?php echo form_input($password,null,"class='input' placeholder='Password'");?>
				<?php echo form_submit('submit', lang('login_submit_btn'),"class='submit'");?>
				<a href="">Forgot Password?</a>
			<?php echo form_close();?>
		</div>
	</body>
	</html>	