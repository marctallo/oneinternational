<div class="small-12 columns flash-message <?php echo $class; ?> callout" data-closable="fade-out">
  <p><?php echo $message; ?></p>
  <button class="close-button" aria-label="Dismiss alert" type="button" data-close>
    <span aria-hidden="true">&times;</span>
  </button>
</div>