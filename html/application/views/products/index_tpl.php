<div class="product-list">

	<div class="row">
		<div class="small-12 columns">
			<table class="product-table">
				<thead>
					<tr>
						<td>Product ID</td>
						<td>Product Name</td>
						<td></td>
					</tr>
				</thead>
				<tbody>
				
					<?php if(count($products) > 0) : ?>
						<?php foreach($products as $product) : ?>
							<tr>
								<td>
									<?php echo html_escape($product->id); ?>
								</td>
								<td>
									<?php echo html_escape($product->product_name); ?>
								</td>
								<td><a href="<?php echo base_url(); ?>admin/products/edit/<?php echo $product->id; ?>"><i class="fa fa-edit"></i></a> <a href="<?php echo base_url(); ?>admin/products/delete/<?php echo $product->id; ?>" onclick="return confirm('Delete this product?');"><i class="fa fa-trash"></i></a></td>
							</tr>
						<?php endforeach; ?>
					<?php else : ?>
						<tr>
							<td colspan="3" class="text-center">No available products</td>
						</tr>
					<?php endif; ?>
					
				</tbody>
			</table>
		</div>
	</div>
	<div class="row">
		<div class="small-12 medium-6 columns"><a href="<?php echo base_url(); ?>admin/products/add">New Product</a></div>
		<div class="small-12 medium-6 columns">
		  <?php echo $this->pagination->create_links(); ?>
		</div>
	</div>

</div>