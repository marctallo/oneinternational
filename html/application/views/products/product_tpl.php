<div class="three small-6 medium-3 columns content-box <?php echo $is_last; ?> product-container">
	<div class="product">
		<img src="<?php echo ($image == 'no-image.png') ? base_url().'assets/images/no-image.png' : base_url().'assets/images/products/thumbnail/'.$image; ?>" alt="<?php echo html_escape($product_name); ?>">
		<div class="overlay">
			<a href="<?php echo base_url()."products/".html_escape($slug); ?>"><i class="fa fa-search"></i></a>
		</div>
	</div>
	<p class="product-title"><?php echo html_escape($product_name); ?></p>
</div>