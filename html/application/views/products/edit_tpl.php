<div class="product-form">
  <?php $url = (isset($product)) ? "admin/products/edit/".$product->id : "admin/products/add" ; ?>
  <?php echo form_open($url); ?>
    <div class="row">
      <div class="small-12 columns">
        <label>Product Name
          <?php echo form_input("product_name",(set_value("product_name")!="") ? set_value("product_name") :(isset($product->product_name) ? $product->product_name : ''),"placeholder='Product Name'"); ?>
        </label>
        <?php echo form_error('product_name'); ?>
      </div>
    </div>
    <div class="row">
      <div class="small-12 columns">
        <label>Slug
          <?php echo form_input("slug",(set_value("slug")!="") ? set_value("slug") :(isset($product->slug) ? $product->slug : ''),"placeholder='Slug'"); ?>
        </label>
        <?php echo form_error('slug'); ?>
      </div>
    </div>
    <div class="row">
      <div class="small-12 medium-9 columns">
        <label>Description
         <textarea name="description"><?php echo (isset($product->description)) ? $product->description :set_value('description'); ?></textarea>
        </label>
      </div>
      <div class="small-12 medium-3 columns">
          <button type="button" class="select-image button" id="select-image" data-open="image-select-modal">set product image</button>
      
          <?php echo form_input(array('name' => 'image', 'type'=>'hidden', 'id' =>'image-name','value'=>(isset($product->image)) ? $product->image :set_value('image'))); ?>
            <img class="product-image" src="<?php echo base_url(); ?>assets/images/<?php echo (isset($product->image)) ? (($product->image =='no-image.png') ? $product->image : 'products/'.$product->image) : ((set_value('image')=='') ? 'no-image.png' : 'products/'.set_value('image')); ?>">
        
      </div>
    </div>
    <div class="row">
      <div class="small-12 columns">
        <?php echo form_submit('submit', 'save',"class='button'"); ?>
      </div>
    </div>
    <?php echo form_close(); ?>

</div>