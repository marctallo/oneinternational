<?php echo form_open("auth/login");?>
	<img src="<?php echo base_url(); ?>assets/images/logo.png" alt="">
	<?php echo form_input($identity,null,"class='input' placeholder='Email'");?>
	<?php echo form_input($password,null,"class='input' placeholder='Password'");?>
	<button type="submit" class="submit primary-btn"><span class="button-value">Login</span> <i class="fa fa-repeat button-loading"></i></button> 
	<a href="<?php echo base_url(); ?>auth/forgot_password">Forgot Password?</a>
<?php echo form_close();?>