<?php echo form_open('auth/reset_password/' . $code);?>
	<img src="<?php echo base_url(); ?>assets/images/logo.png" alt="">
	<?php echo form_input($new_password);?>
	<?php echo form_input($new_password_confirm);?>
	<?php echo form_input($user_id);?>
	<button type="submit" class="submit primary-btn"><span class="button-value">Change Password</span> <i class="fa fa-repeat button-loading"></i></button> 
<?php echo form_close();?>