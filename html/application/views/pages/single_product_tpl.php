
 <section class="single-product">

	<div class="row">
		<div class="five small-12 medium-5 small-push-12 columns image-box">
			<img src="<?php echo ($product->image == 'no-image.png') ? base_url().'assets/images/no-image.png' : base_url().'assets/images/products/'.$product->image; ?>" alt="Product">
		</div>
		<div class="seven small-12 medium-7 small-pull-12 columns">
			<h1 class="title"><?php echo html_escape($product->product_name); ?></h1>
			<hr>
			<div class="description">
				<?php echo $product->description; ?>
			</div>
		</div>
	</div>
</section>