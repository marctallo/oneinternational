<!doctype html>
<html class="no-js" lang="en">

<head>
	<meta charset="utf-8" />
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<title>404 Page Not Found</title>
	<link rel="icon" type="image/png" sizes="32x32" href="<?php echo base_url(); ?>assets/images/favicon-32X32.png">
	<link href="https://fonts.googleapis.com/css?family=Lato:400,700" rel="stylesheet">
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/style.css" />
</head>

<body>
	<div class="page-404">
		<div class="content-box">
			<p class="primary"><span class="highlight">404</span> Page Not Found</p>
			<p class="secondary">We're sorry, but the page you're looking for can't be found.</p>
		</div>
	</div>
</body>

</html>