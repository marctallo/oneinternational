<section class="contact-us">
	<div class="row">
		<div class="twelve small-12 columns">
			<h1>Contact <span class="highlight">Us</span></h1>
		</div>
	</div>
	<div class="row">
		<div class="six small-12 medium-6 columns">
			<ul class="contact-details">
				<li>
					<p><img class="logo" src="<?php echo base_url(); ?>assets/images/favicon-32x32.png"><strong>One Nations Enterprises International Inc</strong></p>
				</li>
				<li>
					<p><i class="fa fa-map-marker"></i>Unit 9 GF N. Dela Merced Bldg., West Avenue, Quezon City</p>
				</li>
				<li>
					<p><i class="fa fa-envelope"></i>oneintlinc@gmail.com</p>
				</li>
			</ul>
		</div>
		<div class="six small-12 medium-6 columns map-container">
			<div class="overlay"></div>
			<iframe width="100%" height="500" frameborder="0" style="border:0" src="https://www.google.com/maps/embed/v1/place?q=14.638439%2C%20121.026384&key=AIzaSyBmxgsDA_zPyMNqhD9r2_yO3k1Wczl5mVA" allowfullscreen></iframe>

		</div>
	</div>
</section>