<div class="about">
  <section class="about-us">
    <div class="row small-12 columns">
      <h1>About <span class="highlight">Us</span></h1>
      <p>A Filipino owned corporation established this June 18, 2016 in Quezon City, Philippines. Engaged in the business of trading of goods such as Marketing of several Consumer Products on wholesale /retail and Product Distributorship of Food Supplements, Cosmetics, Pharmaceutical Products, Beverages and other food commodities without engaging in investment, solicitation nor investment taking activity from public investors. With the growing competition in the industry, One Nations Enterprises International, Inc., has proven its solid foundation through its products known for its three distinct characteristics- <em class="highlight"><strong>quality</strong></em>, <em class="highlight"><strong>effectiveness</strong></em> and <em class="highlight"><strong>affordability</strong></em>. The solid and growing number of members (distributors) is a testament to the competitiveness and salability of the product due to the company’s investment on its Research and Development that continuously sustain innovation both in product and business operation</p>
    	<p>One Nations Enterprises International, Inc. places high regard to its hardworking and diligent members (distributors) by introducing a simple yet efficient scheme. Such scheme entails approach that aims to provide a less complicated and realistic means of generating profits and returns as well as exciting rebates and earnings on a daily, weekly and monthly sales. This is the company’s ways of ensuring that profits are equitably shared between members and the company.
	As the company owes its success to its members, ONE Intl., Inc. provides incentive packages and benefits as an expression of its appreciation as well as to motivate and encourage members. 
</p>
    </div>
  </section>
  <section class="about-mission">
    <div class="row">
      <div class="six small-12 medium-6 columns">
        <h2>Our <span class="highlight">Mission</span></h2>
        <ul>
        	<li>
        		<p><i class="fa fa-arrow-circle-right"></i> A company dedicated to provide a wide spectrum of health, wellness and beauty products which qualities corresponds to local and international standard.</p>
        	</li>
        	<li>
        		<p><i class="fa fa-arrow-circle-right"></i> To expand operations throughout the country and selected international cities to provide entrepreneurial and financial opportunities to fellow Filipinos.</p>
        	</li>
        	<li>
        		<p><i class="fa fa-arrow-circle-right"></i> To strengthen its research and development to generate profits and fair return to members and to finance continued growth and development in quality products.</p>
        	</li>
        </ul>
       
      </div>
      <div class="six small-12 medium-6 columns">
        <h2>Our <span class="highlight">Vision</span></h2>
        <p>To be a recognized international distributor of organic health and wellness products known for its quality, effectiveness and affordability.</p>
      </div>
    </div>
    <div class="row">
    	
    </div>
  </section>
  <section class="about-documents">
    <div class="row">
      <div class="twelve small-12 columns">
        <h2>Company <span class="highlight">Documents</span></h2>
      </div>
    </div>
    <div class="row">
      <div class="three small-12 medium-3 columns">
        <div class="content-box">
          <a class="cbox-gallery" href="<?php echo base_url(); ?>assets/images/documents/document-1.jpg"><img src="<?php echo base_url(); ?>assets/images/documents/thumbnails/document-thumb-1.jpg" alt="FDA Licence"></a>
        </div>
      </div>
      <div class="three small-12 medium-3 columns">
        <div class="content-box">
          <a class="cbox-gallery" href="<?php echo base_url(); ?>assets/images/documents/document-2.jpg"><img src="<?php echo base_url(); ?>assets/images/documents/thumbnails/document-thumb-2.jpg" alt="Certificate of Registration"></a>
        </div>
      </div>
      <div class="three small-12 medium-3 columns">
        <div class="content-box">
          <a class="cbox-gallery" href="<?php echo base_url(); ?>assets/images/documents/document-3.jpg"><img src="<?php echo base_url(); ?>assets/images/documents/thumbnails/document-thumb-3.jpg" alt="Certificate of filing of amended articles of incorporation"></a>
        </div>
      </div>
      <div class="three small-12 medium-3 columns">
        <div class="content-box">
          <a class="cbox-gallery" href="<?php echo base_url(); ?>assets/images/documents/document-4.jpg"><img src="<?php echo base_url(); ?>assets/images/documents/thumbnails/document-thumb-4.jpg" alt="Certificate of incorporation"></a>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="three small-12 medium-3 columns">
        <div class="content-box">
          <a class="cbox-gallery" href="<?php echo base_url(); ?>assets/images/documents/document-5.jpg"><img src="<?php echo base_url(); ?>assets/images/documents/thumbnails/document-thumb-5.jpg" alt="Unified registration record"></a>
        </div>
      </div>
      <div class="three small-12 medium-3 columns">
        <div class="content-box">
          <a class="cbox-gallery" href="<?php echo base_url(); ?>assets/images/documents/document-6.jpg"><img src="<?php echo base_url(); ?>assets/images/documents/thumbnails/document-thumb-6.jpg" alt="Business Permit"></a>
        </div>
      </div>
      <div class="three small-12 medium-3 columns">
        <div class="content-box">
          <a class="cbox-gallery" href="<?php echo base_url(); ?>assets/images/documents/document-7.jpg"><img src="<?php echo base_url(); ?>assets/images/documents/thumbnails/document-thumb-7.jpg" alt="Business Tax Bill"></a>
        </div>
      </div>
      <div class="three small-12 medium-3 columns">
        <div class="content-box">
          <a class="cbox-gallery" href="<?php echo base_url(); ?>assets/images/documents/document-8.jpg"><img src="<?php echo base_url(); ?>assets/images/documents/thumbnails/document-thumb-8.jpg" alt="Official Receipt"></a>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="three small-12 medium-3 columns end">
        <div class="content-box">
          <a class="cbox-gallery" href="<?php echo base_url(); ?>assets/images/documents/document-9.jpg"><img src="<?php echo base_url(); ?>assets/images/documents/thumbnails/document-thumb-9.jpg" alt="Bir"></a>
        </div>
      </div>
    </div>
  </section>
</div>