<section class="hero">
    
    <?php if($is_legacy) : ?>
    
     <div id="featured">
        <img src="<?php echo base_url(); ?>assets/images/slider/orbit-1.jpg" alt="slide image">
        <img src="<?php echo base_url(); ?>assets/images/slider/orbit-2.jpg" alt="slide image">
        <img src="<?php echo base_url(); ?>assets/images/slider/orbit-3.jpg" alt="slide image">
      </div>
      
    <?php else : ?> 
      <div class="orbit" role="region" aria-label="Favorite Space Pictures" data-orbit data-options="animInFromLeft:fade-in; animInFromRight:fade-in; animOutToLeft:fade-out; animOutToRight:fade-out; timerDelay:3000;">
        <ul class="orbit-container">
          <button class="orbit-previous"><span class="show-for-sr">Previous Slide</span>&#9664;&#xFE0E;</button>
          <button class="orbit-next"><span class="show-for-sr">Next Slide</span>&#9654;&#xFE0E;</button>
          <li class="is-active orbit-slide">
            <img class="orbit-image" src="<?php echo base_url(); ?>assets/images/slider/orbit-1.jpg" alt="Space">
          </li>
          <li class="orbit-slide">
            <img class="orbit-image" src="<?php echo base_url(); ?>assets/images/slider/orbit-2.jpg" alt="Space">
          </li>
          <li class="orbit-slide">
            <img class="orbit-image" src="<?php echo base_url(); ?>assets/images/slider/orbit-3.jpg" alt="Space">
          </li>
        </ul>
      </div>
     <?php endif; ?> 
  </section>
  <section class="company-vision">
    <div class="row">
      <div class="four small-12 medium-4 columns content-box">
        <i class="fa fa-group"></i>
        <h3>Mission</h3>
        <p>A company dedicated to provide a wide spectrum of health, wellness and beauty products which qualities corresponds to local and international standard.</p>
      </div>
      <div class="four small-12 medium-4 columns content-box">
        <i class="fa fa-flag"></i>
        <h3>Vision</h3>
        <p>To be a recognized international distributor of organic health and wellness products known for its quality, effectiveness and affordability.</p>
      </div>
      <div class="four small-12 medium-4 columns content-box">
        <i class="fa fa-leaf"></i>
        <h3>Products</h3>
        <p>We want to give the best of both worlds to our members. Be Healthy and Wealthy at the same time.</p>
      </div>
    </div>
  </section>