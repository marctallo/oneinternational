'use strict';
 
var gulp = require('gulp');
var sass = require('gulp-sass');
var prefix      = require('gulp-autoprefixer');
var uglify      = require('gulp-uglify');
var plumber = require('gulp-plumber');


gulp.task('sass', function() {
    return gulp.src("src/scss/**/*.scss")
        .pipe(sass({outputStyle : 'compressed'}).on('error', sass.logError))
        .pipe(prefix("last 5 version", "> 1%", "ie 8", "ie 7"))
        .pipe(gulp.dest("assets/css"));
});

gulp.task('compress', function() {
  return gulp.src('src/js/**/*.js')
		.pipe(plumber())
    .pipe(uglify())
    .pipe(gulp.dest('assets/js'));
});


gulp.task('watcher', ['sass','compress'], function() {

    gulp.watch("src/scss/**/*.scss", ['sass']);
    gulp.watch("src/js/**/*.js", ['compress']);
});


gulp.task('default', ['watcher']);