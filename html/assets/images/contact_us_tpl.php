<section class="breadcrumbs">
    <div class="row">
      <div class="small-12 columns">
        <p><a href="#">Home</a> > <a href="#">Products</a></p>
      </div>
    </div>
  </section>
     <section class="contact-us">
      <div class="row">
        <div class="small-12 columns">
          <h1>Contact <span class="highlight">Us</span></h1>
        </div>
      </div>
      <div class="row">
        <div class="small-12 medium-6 columns">
          <ul class="contact-details">
            <li><p><img class="logo" src="<?php echo base_url(); ?>assets/images/favicon-32X32.png"><strong>One Nations Enterprises International Inc</strong></p></li>
            <li><p><i class="fa fa-map-marker"></i>GF Delta Bldg., West Avenue, Quezon City</p></li>
            <li><p><i class="fa fa-phone"></i>0932-4323456, 0988-4567856</p></li>
            <li><p><i class="fa fa-envelope"></i>email@email.com</p></li>
          </ul>
        </div>
        <div class="small-12 medium-6 columns map-container">
        <div class="overlay"></div>
         <iframe width="100%" height="500" frameborder="0" style="border:0"
src="https://www.google.com/maps/embed/v1/place?q=GF%20Delta%20Bldg.%2C%20West%20Avenue%2C%20Quezon%20City&key=AIzaSyCQtTRXOmDX_o6pQCdRoxay20gzpyI0S5o&amp" allowfullscreen></iframe>
     
        </div>
      </div>
    </section>