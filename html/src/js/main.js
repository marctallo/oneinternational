$(document).foundation();

$(document).ready(function(){

	page.init();
});


var page = (function(){
	
	'use strict';
	
	var menu_btn,
			menu,
			product_row;
	
	
	function setVar(){
		
		menu_btn 	= $('.mobile-menu');
		menu			= $('.menu');
		product_row = $('.product-row');
	}
	
	
	/**
		* Initialize hamburger menu for mobile
		*
		* @return null
		*
		*/
	function setMobileMenu(){
		
		menu_btn.click(function(){
			
			$(this).toggleClass('open');
			
			if(menu.hasClass('active')){
				menu.animate({height:0},300);
				menu.removeClass('visible');
				setTimeout(function(){
					menu.removeClass('active').removeAttr('style');
				},320);
			}else{

				var new_height = menu.css('height','auto').height();

				menu.height(0).animate({height:new_height},300);
				menu.addClass('visible');

				setTimeout(function(){

					menu.addClass('active').removeAttr('style');

				},320);
			}
			
		});
		
	}
	
	/**
		* Disable color box on mobile view
		*
		* @return null
		*
		*/
	function disableColorBoxOnMobile(){
		
		if ($(window).width() >= 640){	
			$('.cbox-gallery').colorbox();
		}else{
			$('.cbox-gallery').click(function(e){
				e.preventDefault();
			});
		}
	}
	
	function setButtonLoading(){
		
		$('.submit').click(function(){
			
			var self = $(this);
			
			
			self.find('.button-value').text("Loading... ");
			self.find('.button-loading').css('display','inline-block');
			self.closest('form').submit();
			self.attr('disabled',true);
			
			
			
		});
	}
	
	function equalizeProductHeight(){
				
				$('.product-container').removeAttr('style');
		
				product_row.each(function(){

					var self = $(this),
							tallest = 0,
							content = self.find('.product-container');

					content.each(function(){

						if($(this).outerHeight() > tallest){

							tallest = $(this).outerHeight();
							
						}
					});

					content.outerHeight(tallest);
					
				});
	}
	
	function init(){
		
		setVar();
		setMobileMenu();
		disableColorBoxOnMobile();
		setButtonLoading();
		$(window).bind("load", equalizeProductHeight);
		$(window).bind("resize", equalizeProductHeight);
	}
	
	return {
		
		init : init
	}
	
})();
