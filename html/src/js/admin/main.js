$(document).ready(function(){

	page.init();
	
});

/**
	* Set the sidebar equals to the height of the main content
	*
	* @return void
	*
	*/

function setSidebarHeight(){ //todo
	
	var sidebar = $('.sidebar');
	var main_content = $('.main-content').outerHeight(true);
	
  sidebar.css('height',main_content+'px');
	console.log(sidebar.css('height')+" "+main_content);
}
	
	var page = (function(){

		'use strict';
		
		var base_url = $('#base_url').val();
		
		
		function dropzone(){
			
			Dropzone.options.imageDropzone = {
				
				paramName : "media",
				maxFilesize: 20,
				acceptedFiles: ".jpeg,.jpg,.png",
				init: function() {
					this.on('sending', function(file, xhr, formData){
						
						formData.append('onetoken', $('[name="onetoken"]').val());
					});
					
				}
    		
			}
		}

		function imageSelector(){
			
			
			var image_thumb =  $('.reveal .modal-content .image-thumb'),
				image_name = '',
				modal = $('.reveal'),
				modal_content = $('.modal-content'),
				load_modal_content = true;
			
			$(document).on('click','.reveal .modal-content .image-thumb',function(){

					var self = $(this);

					if(self.hasClass('selected')){

						self.removeClass('selected');
						image_name = '';
					}else {

						$('.image-thumb').removeClass('selected');
						self.addClass('selected');

						image_name = (self.attr('src')).split("/");
					}

					$('#image-name').val(image_name[(image_name.length)-1]);

			});
			
			$('#set-image').click(function(){
				
				var image_src;
				
				modal.foundation('close');
				
				if($('#image-name').val() != ""){
					image_src = base_url+'assets/images/products/'+$('#image-name').val();
				}else{
				
					image_src = base_url+'assets/images/no-image.png';
				}
				
				
				$('.product-image').attr('src',image_src);
				
			});
			
			
			modal.on('open.zf.reveal',function(){
				
				
				if(load_modal_content){
					$.ajax($('#base_url').val()+'media/getimagenames')
						.done(function(data){

						var content = "<div class='row'>",
								is_last = '';

						if(data.length > 0){

							for(var i = 0; i < data.length; i++){
								
								if(i == data.length-1){
									
									is_last = "end";
								}
								content+="<div class='small-12 medium-2 columns "+is_last+" '><img class='image-thumb' src='"+base_url+"assets/images/products/"+data[i]+"' alt='"+data[i]+"'></div>";
							}

							content +="</div>";

							modal_content.html(content);
							load_modal_content = false;
							
						}

					});
					
				}
				
			});
			
		}
		//initialize the page
		function init(){

			//initialize foundation
			$(document).foundation();

			//initialize tinymce
			tinymce.init({
				selector:"textarea",
				toolbar:"sizeselect | fontsizeselect | insertfile undo redo | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent",
				fontsize_formats:"8pt 10pt 12pt 14pt 18pt 24pt 36pt",
				menubar:false,
        height :300
			});
			
			//initialize image modal
			imageSelector();
			
			//initialize dropzone
			dropzone();
		}

		return {

			init:init
		}


	})();
